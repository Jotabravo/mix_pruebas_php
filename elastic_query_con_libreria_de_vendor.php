<?php
require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
include 'elastic_config.php';

use Elastic\Api\Client;

$c = new Client($elastic_server['elastic103'], 'log-system-watcher*');

$dt_start = strtotime("-3800 min")*1000; //Tiempo inicial para query hacia el API Elastic S.
$dt_end = strtotime("now")*1000;
$size = 10000; //Es un valor bajo criterio propio, pero debe ser menor igual a 10000, pues así está configurado en la aplicación.

$config = array(
    'method' => 'GET',
    'postFields' => array(
        'query' => array(
            'bool' => array(
                'must' => array(
                    array(
                        'range' => array(
                            '@timestamp' => array(
                                'gte' => $dt_start,
                                'lte' => $dt_end,
                                'relation' => 'within'
                            )
                        )
                    )
                )
            )
        ),
        'sort' => array(
        	'@timestamp' => 'DESC'
    	),
        'size' => $size
    ),
    'output' => 'array',
    'headers' => 'json'
);

$c->setConfig($config);
$c->setActionUrlSuffix('_search?pretty');

$data = $c->handleData();

$total = $data['total'];
$data = $data['data'];

echo $total . PHP_EOL;

foreach ($data as $key => $value) {
	var_dump($value);
}
