<?php
    function htmlMessage($titulo, $columnas, $total_columnas, $filas)
    {
		$html = "";
		$html.= "<h2>";
		$html.= $titulo;
		$html.="</h2>";	
		$html.= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse; border:solid black 1.0pt' width='1200'>";
			$html.= "<tbody>";
				$html.= "<tr style='background:rgb(221,221,221);'>";
					foreach ($columnas as $key => $value) {
						if ($key == 0) {
							$html.= "<td style='border:solid black 1.0pt; background:rgb(221,221,221); padding:5px 5px 5px 5px'>";
						} else {
							$html.= "<td style='border:solid black 1.0pt; border-left:none; background:rgb(221,221,221); padding:5px 5px 5px 5px'>";
						}

							$html.="<div align='center' style='text-align:center;margin:0;'>";
								$html.="<font face='Times New Roman,serif' size='3'>";
									$html.="<span style='font-size:12pt;'>";
										$html.="<font size='3'>";
											$html.= "<span style='font-size:11pt'>";
												$html.= "<b>";
													$html.= $value;
												$html.= "</b>";
											$html.= "</span>";
										$html.="</font>";
									$html.="</span>";
								$html.="</font>";
							$html.="</div>";
						$html.= "</td>";
					}
				$html.= "</tr>";

				foreach ($filas as $key => $value) {
					$html.= "<tr>";
						for ($i = 0; $i < $total_columnas; $i++) { 
							$html.= "<td style='border:solid black 1.0pt; padding:5px 5px 5px 5px; border-collapse: collapse;'>";
									$html.="<div><font><span style='font-size:11pt'><font>";
										$html.= $value[$i];
									$html.="</font></span></font></div>";
							$html.= "</td>";
						}
					$html.= "</tr>";
				}

			$html.= "</tbody>";
		$html.= "</table>";

		return $html;
    }

    $titulo = "Alerta Heartbeat Uptime monitoring";

	$columnas = array(
		"Monitor status",
		"Last updated",
		"Hostname",
		"Domain",
		"Port",
		"Servicio",
		"Aplicación",
		"Plataforma",
		"Latest message"
	);

	$total_columnas = count($columnas);

    $filas = array(
		array(
			"DOWN",
			"10/05/2019 14:54:10",
			"elasticv5",
			"tcp:10.10.10.2:3306",
			"3306",
			"Nombre del servicio",
			"Nombre de la aplicación",
			"Nombre de la plataforma",
			"Error message"
		),
		array(
			"DOWN",
			"10/05/2019 14:54:10",
			"elasticv5",
			"tcp:10.10.10.2:3306",
			"3306",
			"Nombre del servicio",
			"Nombre de la aplicación",
			"Nombre de la plataforma",
			"Error message"
		),
		array(
			"DOWN",
			"10/05/2019 14:54:10",
			"elasticv5",
			"tcp:10.10.10.2:3306",
			"3306",
			"Nombre del servicio",
			"Nombre de la aplicación",
			"Nombre de la plataforma",
			"Error message"
		)
	);

	echo htmlMessage($titulo, $columnas, $total_columnas, $filas);