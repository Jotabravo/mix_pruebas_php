<?php

$server_ip = '172.16.102.101';
$server_port = '9200';
$server_url = "http://{$server_ip}:{$server_port}";
$server_user = 'elastic';
$server_password = 'claro123';
$tipo = 1; //1 --> GET, 2 --> POST
$index = "prueba_*";

//_update_by_query example 1
/*curl -XPOST '<elasticesarch_uri>/<index>/_update_by_query?pretty' -d 
'{
    "query": {
        "match": {
            "phenomenon": "eclipse"
        }
    },
    "script": {
        "lang": "painless",
        "source": "ctx._source.phenomenon = 'lunar_eclipse'"
    }
}'*/

//_update_by_query example 2
/*curl -XPOST "localhost:9200/seats/_update_by_query?pretty" -H 'Content-Type: application/json' -d
'{
    "query": {
        "bool": {
            "filter": [
            {
                "range": {
                    "row": {
                        "lte": 3
                    }
                }
            },
            {
                "match": {
                    "sold": false
                }
            }]
        }
    },
    "script": {
        "source": "ctx._source.cost -= params.discount",
        "lang": "painless",
        "params": {
            "discount": 2
        }
    }
}'*/

//simple update example 1
/*curl -XPOST '<your_elasticesarch_uri>/<index>/<type>/<doc_id>/_update' -H 'Content-Type: application/json' -d
'{ 
   "field : "new_value", 
   ... 
}'*/

//simple update example 2
/*POST /catalog/product/1/_update
{
    "doc": {
        "author": "Albert Paro",
        "title": "Elasticsearch 5.0 Cookbook",
        "description": "Elasticsearch 5.0 Cookbook Third Edition",
        "price": "54.99"
    }
}*/

//simple update example 3
/*POST /catalog/product/AVrASKqgaBGmnAMj1SBe/_update
{
    "script": {
        "source": "ctx._source.price += params.increment",
        "lang": "painless",
        "params": {
            "increment": 2
        }
    }
}*/

//ejemplo probado en la IP 172.16.102.101 desde la herramienta Dev Tools de Kibana:

//POST /prueba_*/_update_by_query?pretty
/*{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "uploaded_by": "Un cambio usando params desde script php - con source //// desde script php"
          }
        }
      ]
    }
  },
  "script": {
    "lang": "painless",
    "source": "ctx._source.uploaded_by = params.new_uploaded_by; ctx._source.likes = params.likes;",
    "params": {
      "new_uploaded_by": "Original con likes",
      "likes": 1
    }
  }
}*/

if ($tipo == 1) {
    /************* GET: Prueba con prueba_update_data_es *****************/
    $uploaded_by = "Lucidworks";

    $config = array(
        'method' => 'GET',
        'postFields' => array(
            'query' => array(
                'bool' => array(
                    'must' => array(
                        array(
                            'match' => array(
                                'uploaded_by' => $uploaded_by
                            ),
                        )
                    )
                )
            )
        ),
        'output' => 'array',
        'headers' => 'json'
    );

    $url = "$server_url/$index/_search?pretty";
} else {
    /************* UPDATE: Prueba con prueba_update_data_es *****************/
    $uploaded_by = "Ciprian Hacman";
    $uploaded_by_new = "Original con likes + de un parámetro";
    $xcodigo_respuesta = "1";
    $xmensaje_respuesta = 'Operacion Exitosa #2';
    $new_field = 'refresh included';

    $config = array(
        'method' => 'POST',
        'postFields' => array(
            'query' => array(
                'bool' => array(
                    'must' => array(
                        array(
                            'match' => array(
                                'uploaded_by' => $uploaded_by
                            )
                        )
                    ),
                    'must_not' => array(
                        'exists' => array(
                            'field' => 'xcodigo_respuesta'
                        )
                    )
                )
            ),
            'script' => array(
                'lang' => "painless",
                //'source' => "ctx._source.uploaded_by = params.new_uploaded_by; ctx._source.likes = params.likes;",
                //'source' => "ctx._source.uploaded_by = params.new_uploaded_by; ctx._source.likes += 1;",
                //'source' => "ctx._source.title = ctx._source.title + ' [' + ctx._source.uploaded_by + ']'; ctx._source.likes += 1;",
                //'params' => array(
                //    'new_uploaded_by' => $uploaded_by_new,
                //    "likes" => 2
                //)
                //'source' => "ctx._source.xcodigo_respuesta = params.xcodigo_respuesta; ctx._source.xmensaje_respuesta = params.xmensaje_respuesta;",
                //'params' => array(
                //    'xcodigo_respuesta' => $xcodigo_respuesta,
                //    "xmensaje_respuesta" => $xmensaje_respuesta
                //)
                'source' => "ctx._source.new_field = params.new_field;",
                'params' => array(
                    'new_field' => $new_field
                )
            )
        ),
        'output' => 'array',
        'headers' => 'json'
    );

    $url = "$server_url/$index/_update_by_query?pretty";
    //$url = "$server_url/$index/_update_by_query?refresh&pretty";
}

ini_set('memory_limit', '4096M'); // Se coloca esto si aparece el error de Exhausted Memory en PHP

echo "Llamando a URL $url" . PHP_EOL;

$hasConfig = isset($config) && !empty($config);
$hasPostFields = $hasConfig && isset($config['postFields']);
$hasHeaders = $hasConfig && isset($config['headers']);
$hasMethod = $hasConfig && isset($config['method']);
$hasOutput = $hasConfig && isset($config['output']);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, $hasOutput ? 1 : 0);

if ($hasPostFields)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['postFields']));

if ($hasMethod)
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($config['method']));
else
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

curl_setopt($ch, CURLOPT_USERPWD, "{$server_user}:{$server_password}");

if ($hasHeaders) {
    if (is_string($config['headers']) && $config['headers'] === 'json') {
        $headers = array("Content-Type: application/json");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    } elseif (is_array($config['headers'])) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $config['headers']);
    }
}

$result = curl_exec($ch);

if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch) . PHP_EOL;
}

curl_close($ch);

if ($hasOutput) {
    if ($config['output'] === 'object')
        $result = json_decode($result);
    if ($config['output'] === 'array')
        $result = json_decode($result, true);
}

if ($tipo == 1) {
    $buckets = isset($result["hits"]["hits"]) ? $result["hits"]["hits"] : array();
    $total_buckets = count($buckets);

    if ($total_buckets > 0) {
        echo "$total_buckets elementos encontrados" . PHP_EOL;
        echo PHP_EOL;

        foreach ($buckets as $key => $value) {
            var_dump($value);
        }
    } else {
        echo "No se encontraron resultados";
    }
} else {
    $updated = $result["updated"];

    if ($updated > 0) {
        echo "$updated elemento (s) fue (ron) actualizado (s)";
    } else {
        echo "No se actualizaron elementos";
    }
}
