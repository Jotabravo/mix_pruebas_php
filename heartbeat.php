<?php

$server_ip = '172.16.102.101';
$server_port = '9200';
$server_url = "http://{$server_ip}:{$server_port}";
$server_user = 'elastic';
$server_password = 'claro123';

/************* ICMP *****************/

/*$dt_start = strtotime("-5 min")*1000; //Tiempo inicial para query hacia el API Elastic S.
$dt_end = strtotime("now")*1000;
$reasonable_max_bucket_size = 200; //Por ejemplo: 200
$reasonable_max_top_hits = 100; //La aplicación está configurada para no soportar más de 100.
$min_doc_count = 1;
$size = 0;
$monitor_type = "icmp";
$up_status = 'up';
$down_status = 'down';

$config = array(
	'method' => 'GET',
    'postFields' => array(
    	'query' => array(
            'bool' => array(
                'must' => array(
                	array(
                        'match' => array(
                            'monitor.type' => $monitor_type
                        )
                    ),
                    array(
                        'range' => array(
                            '@timestamp' => array(
                                'gte' => $dt_start,
                                'lte' => $dt_end,
                                'relation' => 'within'
                            )
                        )
                    )
                )
            )
        ),
        'aggregations' => array(
        	'group_by_host' => array(
        		'terms' => array(
        			'field' => 'url.full',
        			'min_doc_count' => $min_doc_count,
        			'size' => $reasonable_max_bucket_size
        		),
        		'aggregations' => array( # Sub-aggregations
        			'total' => array(
						'value_count' => array(
							'field' => 'url.full'
						)
					),
					'total_up' => array(
						'filter' => array(
							'term' => array(
								'monitor.status' => $up_status
							)
						),
	            		'aggregations' => array( # Sub-aggregations
        					'count_up' => array(
        						'value_count' => array(
        							'field' => 'monitor.status'
        						)
        					)
        				)
					),
					//'total_down' => array(
					//	'filter' => array(
					//		'term' => array(
					//			'monitor.status' => $down_status
					//		)
					//	),
	            	//	'aggregations' => array( # Sub-aggregations
        			//		'count_down' => array(
        			//			'value_count' => array(
        			//				'field' => 'monitor.status'
        			//			)
        			//		)
        			//	)
					//),
					'total_up_percentage' => array(
						'bucket_script' => array(
							'buckets_path' => array(
								'totalUp' => 'total_up.count_up',
								'totalGlobal' => 'total',
							),
							'script' => 'params.totalUp / params.totalGlobal * 100'
						)
					)
        		)
        	)
        ),
        'sort' => array(
        	'@timestamp' => 'DESC'
      	),
    	'size' => $size,
	),
	'output' => 'array',
    'headers' => 'json'
);*/

/************* TCP *****************/

$url_full = "tcp://172.16.102.104"; //Para probar
$dt_start = strtotime("-5 min")*1000; //Tiempo inicial para query hacia el API Elastic S.
$dt_end = strtotime("now")*1000;
$reasonable_max_bucket_size = 200; //Por ejemplo: 200
$reasonable_max_top_hits = 100; //La aplicación está configurada para no soportar más de 100.
$min_doc_count = 1;
$size = 0;
$monitor_type = "tcp";
$up_status = 'up';
$down_status = 'down';

$config = array(
	'method' => 'GET',
    'postFields' => array(
    	'query' => array(
            'bool' => array(
                'must' => array(
                	array(
                        'match' => array(
                            'monitor.type' => $monitor_type
                        )
                    ),
                    array(
                    	'wildcard' => array(
                        	'url.full' => $url_full.'*'
                        )
                    ),
                    array(
                        'range' => array(
                            '@timestamp' => array(
                                'gte' => $dt_start,
                                'lte' => $dt_end,
                                'relation' => 'within'
                            )
                        )
                    )
                )
            )
        ),
		'aggregations' => array(
        	'group_by_host' => array(
        		'terms' => array(
        			'field' => 'url.full',
        			'min_doc_count' => $min_doc_count,
        			'size' => $reasonable_max_bucket_size
        		),
        		'aggregations' => array( # Sub-aggregations
        			'total' => array(
						'value_count' => array(
							'field' => 'url.full'
						)
					),
					//'total_up' => array(
					//	'filter' => array(
					//		'term' => array(
					//			'monitor.status' => $up_status
					//		)
					//	),
	            	//	'aggregations' => array( # Sub-aggregations
        			//		'count_up' => array(
        			//			'value_count' => array(
        			//				'field' => 'monitor.status'
        			//			)
        			//		)
        			//	)
					//),
					'total_down' => array(
						'filter' => array(
							'term' => array(
								'monitor.status' => $down_status
							)
						),
	            		'aggregations' => array( # Sub-aggregations
        					'count_down' => array(
        						'value_count' => array(
        							'field' => 'monitor.status'
        						)
        					)
        				)
					),
					'port' => array(
						'terms' => array(
							'field' => 'url.port'
						)
					),
					'domain' => array(
						'terms' => array(
							'field' => 'url.domain'
						)
					),
					'timestamp' => array(
						'terms' => array(
							'field' => '@timestamp'
						)
					),
					'total_down_percentage' => array(
						'bucket_script' => array(
							'buckets_path' => array(
								'totalDown' => 'total_down.count_down',
								'totalGlobal' => 'total',
							),
							'script' => 'params.totalDown / params.totalGlobal * 100'
						)
					)
        		)
        	)
        ),
        'sort' => array(
        	'@timestamp' => 'DESC'
      	),
    	'size' => $size,
	),
	'output' => 'array',
    'headers' => 'json'
);

$index = "heartbeat-*";
$url = "$server_url/$index/_search?pretty";

ini_set('memory_limit', '4096M'); // Se coloca esto si aparece el error de Exhausted Memory en PHP

echo "Llamando a URL $url" . PHP_EOL;

$hasConfig = isset($config) && !empty($config);
$hasPostFields = $hasConfig && isset($config['postFields']);
$hasHeaders = $hasConfig && isset($config['headers']);
$hasMethod = $hasConfig && isset($config['method']);
$hasOutput = $hasConfig && isset($config['output']);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, $hasOutput ? 1 : 0);

if ($hasPostFields)
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['postFields']));

if ($hasMethod)
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($config['method']));
else
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

curl_setopt($ch, CURLOPT_USERPWD, "{$server_user}:{$server_password}");

if ($hasHeaders) {
    if (is_string($config['headers']) && $config['headers'] === 'json') {
        $headers = array("Content-Type: application/json");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    } elseif (is_array($config['headers'])) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $config['headers']);
    }
}

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch) . PHP_EOL;
}

curl_close($ch);

if ($hasOutput) {
    if ($config['output'] === 'object')
        $result = json_decode($result);
    if ($config['output'] === 'array')
        $result = json_decode($result, true);
}

$buckets = isset($result["aggregations"]["group_by_host"]["buckets"]) ? $result["aggregations"]["group_by_host"]["buckets"] : array();
$total_buckets = count($buckets);

if ($total_buckets > 0) {
	echo "$total_buckets elementos encontrados" . PHP_EOL;
	echo PHP_EOL;

	foreach ($buckets as $key => $value) {
		var_dump($value);
	}
} else {
	echo "No se encontraron resultados";
}
