<?php

$case = 2;
$dirname = "./";
$filename = "unpackexample.txt";
$fullfilename = $dirname . $filename;

$file = fopen($fullfilename, "r") or die("Unable to open file!");

switch ($case) {
	case 1:
		$maxlencol1 = 0;
		$maxlencol2 = 0;
		$maxlencol3 = 0;
		$maxlencol4 = 0;

		while(!feof($file)) {
			while(($cur_line = stream_get_line($file, 0, "\r\n")) !== false) {
				$col1 = substr($cur_line, 0, 12); //Columna 1
				$col2 = substr($cur_line, 12, 10); //Columna 2
				$col3 = substr($cur_line, 22, 11); //Columna 3
				$col4 = substr($cur_line, 33); //Columna 4

				$maxlencol1 = strlen(trim($col1)) > $maxlencol1 ? strlen(trim($col1)) : $maxlencol1;
				$maxlencol2 = strlen(trim($col2)) > $maxlencol2 ? strlen(trim($col2)) : $maxlencol2;
				$maxlencol3 = strlen(trim($col3)) > $maxlencol3 ? strlen(trim($col3)) : $maxlencol3;
				$maxlencol4 = strlen(trim($col4)) > $maxlencol4 ? strlen(trim($col4)) : $maxlencol4;

				echo $col1 . strlen(trim($col1)) . PHP_EOL;
				echo $col2 . strlen(trim($col2)) . PHP_EOL;
				echo $col3 . strlen(trim($col3)) . PHP_EOL;
				echo $col4 . strlen(trim($col4)) . PHP_EOL;
				echo PHP_EOL;
			}
		}

		echo "Tamaño máximo COL1: " . $maxlencol1 . PHP_EOL;
		echo "Tamaño máximo COL2: " . $maxlencol2 . PHP_EOL;
		echo "Tamaño máximo COL3: " . $maxlencol3 . PHP_EOL;
		echo "Tamaño máximo COL4: " . $maxlencol4 . PHP_EOL;

		break;

	case 2:
		while(!feof($file)) {
			while(($cur_line = stream_get_line($file, 0, "\r\n")) !== false) {
				$col1 = trim(substr($cur_line, 0, 12)); //Columna 1
				$col2 = trim(substr($cur_line, 12, 10)); //Columna 2
				$col3 = trim(substr($cur_line, 22, 11)); //Columna 3
				$col4 = trim(substr($cur_line, 33)); //Columna 4

				$cur_arr = array();
				$cur_arr["col1"] = $col1;
				$cur_arr["col2"] = $col2;
				$cur_arr["col3"] = $col3;
				$cur_arr["col4"] = $col4;

				$arr[] = $cur_arr;
			}
		}

		var_dump($arr);

		break;

	default:
		break;
}

fclose($file);