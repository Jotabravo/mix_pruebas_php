<?php

// Declarar la interfaz 'iTemplate'
interface iTemplate
{
    public function setVariable($name, $var);
    public function getVariables();
    public function getHtml($template);
}

// Implementar la interfaz
class Template implements iTemplate
{
    private $vars = array();
  
    public function setVariable($name, $var)
    {
        $this->vars[$name] = $var;
    }

    public function getVariables()
    {
        return $this->vars;
    }
  
    public function getHtml($template)
    {
        foreach($this->vars as $name => $value) {
            $template = str_replace($name, $value, $template);
        }
 
        return $template;
    }
}

$t = new Template();

$array = array(
    "a" => "A",
    "e" => "E",
    "i" => "I",
    "o" => "O",
    "u" => "U"
);

foreach ($array as $key => $value) {
    $t->setVariable($key, $value);
}

//var_dump($t->getVariables());

$template = "Hello World for you!";

print $t->getHtml($template);