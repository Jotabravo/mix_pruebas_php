<?php

$host = "CDR-Converter";
$num_incidencia = "INC000002085090";
$ip = "10.95.203.20, 10.95.203.21, 192.168.122.1";
$total_incidencias_ultimo_mes = 6;

$devices = array(
	array(
		"device_name" => "/dev/mapper/LVL-var",
		"mount_point" => "/var",
		"free" => "53.09G",
		"total" => "196.74G",
		"used_pct" => "100.00",
		"used_bytes" => "188.50G",
		"sobrepasa_umbral" => 1
	),
	array(
		"device_name" => "/dev/mapper/LVL-home",
		"mount_point" => "/home",
		"free" => "91.63G",
		"total" => "147.52G",
		"used_pct" => "39.90",
		"used_bytes" => "55.89G",
		"sobrepasa_umbral" => 0
	),
	array(
		"device_name" => "/dev/sda2",
		"mount_point" => "/boot",
		"free" => "347.33M",
		"total" => "452.96M",
		"used_pct" => "24.80",
		"used_bytes" => "105.62M",
		"sobrepasa_umbral" => 0
	),
	array(
		"device_name" => "/dev/mapper/LVL-root",
		"mount_point" => "/",
		"free" => "123.60G",
		"total" => "155.80G",
		"used_pct" => "21.80",
		"used_bytes" => "32.28G",
		"sobrepasa_umbral" => 0
	)
);

$html= "<h2>";
	$html.= "Hostname: $host";
	$html.= " / $num_incidencia";
$html.="</h2>";
$html.= "<h3>IP Address: $ip</h3>";
$html.= "<h4>Incidencias en los últimos 30 días: $total_incidencias_ultimo_mes</h4>";

$html.= "<table border='1' height='100%' cellpadding='0' cellmargin='0' style='border:1px solid;font-size:9pt;font-family:Calibri;border-collapse:collapse' width='600'>";
	$html.= "<tbody>";
		$html.= "<tr style='background:#dddddd;'>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Device name";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Total";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Used (bytes)";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Free";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Used";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
			$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
				$html.="<div align='center' style='text-align:center;margin:0;'>";
					$html.="<font>";
						$html.="<span style='font-size:12pt;'>";
							$html.= "<span style='font-size:9pt'>";
								$html.= "<b>";
									$html.= "Mount point";
								$html.= "</b>";
							$html.= "</span>";
						$html.="</span>";
					$html.="</font>";
				$html.="</div>";
			$html.= "</td>";
		$html.= "</tr>";

		foreach ($devices as $key => $value) {
			$device_name = $value["device_name"];
			$mount_point = $value["mount_point"];
			$free = $value["free"];
			$total = $value["total"];
			$used_pct = $value["used_pct"];
			$used_bytes = $value["used_bytes"];
			$sobrepasa_umbral = $value["sobrepasa_umbral"];
			
			if ($sobrepasa_umbral == 1) {
				$html.= "<tr style='color: white; font-weight:bold;font-size:9pt;background-color:rgb(255,26,26);'>";
			} else {
				$html.= "<tr style='font-size:9pt'>";
			}

				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $device_name;
					$html.= "</td>";
					$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $total;
					$html.= "</td>";
					$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $used_bytes;
					$html.= "</td>";
					$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $free;
					$html.= "</td>";
					$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $used_pct."%";
					$html.= "</td>";
					$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
						$html.= $mount_point;
					$html.= "</td>";
			$html.= "</tr>";
		}

	$html.= "</tbody>";
$html.= "</table>";

echo $html;
