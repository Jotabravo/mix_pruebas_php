<?php
require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
include 'elastic_config.php';

use Elastic\Api\Client;

$connection = $elastic_server['elastic148'];
$index = 'machine_planos';

$c = new Client($connection, $index);

//Fuente: https://www.elastic.co/guide/en/elasticsearch/reference/current/ml-get-record.html
$start = strtotime("-16 min") * 1000;
$end = strtotime("-45 min") * 1000;
$record_score = 28;

$config = array(
    'method' => 'GET',
    'postFields' => array(
        'sort' => 'timestamp', //DESC
        //'record_score' => $record_score, //Registros mayores iguales a 'record_score'
        'start' => "$start", //Registros cuya fecha es mayor a 'start'
        //'end' => "$end"
        'page' => array(
        	'from' => 0,
        	'size' => 1000
        )
    ),
    'output' => 'array',
    'headers' => 'json'
);

$c->setConfig($config);
$c->setActionUrlSuffix('records?pretty');

$data = $c->handleMlData();

$total = $data['total'];
$data = $data['data'];

echo $total . PHP_EOL;

/*var_dump($data);
exit();*/

foreach ($data as $key => $value) {
	//var_dump($value);
	echo "ANOMALÍA NÚMERO " . ($key + 1) . PHP_EOL;
	echo "Encontrada el " . timeStampToDatetime($value['timestamp']) . " horas" . PHP_EOL;
	echo "Su severidad es " . floor($value['record_score']) . PHP_EOL;
	echo "El plano afectado es ";

	if(array_key_exists('ifalias.keyword', $value)) {
		echo $value['ifalias.keyword'][0];
	} else {
		echo 'NO DEFINIDO';
	}
	
	echo PHP_EOL;
	echo "La IP afectada es ";

	if(array_key_exists('ipaddress.keyword', $value)) {
		echo $value['ipaddress.keyword'][0];
	} else {
		echo 'NO DEFINIDO';
	}

	echo PHP_EOL;
	echo "La severidad actual es " . floor($value['actual'][0]) . PHP_EOL;
	echo "La severidad típica es " . round($value['typical'][0], 1) . PHP_EOL;
	echo PHP_EOL;
}

function timeStampToDatetime($timestamp)
{
	$timestamp = substr($timestamp, 0, 10); //La data original incluye 3 ceros de más al final, por ese se aplica este filtro antes.
	return date('d/m/Y H:i:s', $timestamp);
}
