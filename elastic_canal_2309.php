<?php
    /*curl -u elastic:claro123 -XGET 'http://10.95.164.178:9200/_xpack/ml/anomaly_detectors/canal_2309/results/records?pretty'  -H 'Content-Type: application/json' -d'
    {
    "sort":"record_score"
    }'*/

    try {
        $server_ip = '10.95.164.178';
        $server_port = '9200';
        $server_url = "http://{$server_ip}:{$server_port}";
        $server_user = 'elastic';
        $server_password = 'claro123';
        $job = "_xpack/ml/anomaly_detectors/canal_2309/results";
        $sort = 'record_score';

        $config = array(
            'method' => 'GET',
            'postFields' => array(
                'sort' => $sort
            ),
            'output' => 'array',
            'headers' => 'json'
        );

        $url = "$server_url/$job/records?pretty";

        $hasConfig = isset($config) && !empty($config);
        $hasPostFields = $hasConfig && isset($config['postFields']);
        $hasHeaders = $hasConfig && isset($config['headers']);
        $hasMethod = $hasConfig && isset($config['method']);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($hasPostFields)
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['postFields']));

        if ($hasMethod)
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($config['method']));
        else
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch, CURLOPT_USERPWD, "{$server_user}:{$server_password}");

        if ($hasHeaders) {
            if (is_string($config['headers']) && $config['headers'] === 'json') {
                $headers = array("Content-Type: application/json");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            } elseif (is_array($config['headers'])) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $config['headers']);
            }
        }

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch) . PHP_EOL;
        }

        curl_close($ch);

        $result = json_decode($result, true);

        if (isset($result["status"])) {
            $status = $result["status"];

            if ($status === 404) {
                $reason = $result["error"]["reason"];
                $json = json_encode(array("status" => false, "data" => $reason));
            }
        } else {
            $json = json_encode(array("status" => true, "data" => $result));
        }

        $file = 'C:\xampp\htdocs\prueba-tablas\output_canal_2309.json';
        $file = fopen($file, "w");
        fwrite($file, $json);
        fclose($file);
    } catch (Exception $e) {
        $json = json_encode(array("status" => false, "data" => $e->getMessage()));
    }

    echo $json;
    echo PHP_EOL;
