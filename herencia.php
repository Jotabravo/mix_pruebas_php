<?php

class Foo
{
    public $name;
    public $age;
    private $sexo;

    public function __construct()
    {
        $this->name = "Juan";
        $this->age = 10;
        $this->sexo = "Masculino";
    }

    public function printItem($string)
    {
        echo 'Foo: ' . $string . PHP_EOL;
    }
    
    public function printPHP()
    {
        echo 'PHP is great.' . PHP_EOL;
    }

    public function getSexo()
    {
        return $this->sexo;
    }
}

class bar extends Foo
{
    public function printItem($string)
    {
        echo 'Bar: ' . $string . PHP_EOL;
    }

    public function getPerson()
    {
        echo 'Nombre: ' . $this->name . '. Edad: ' . $this->age . '. Sexo: ' . $this->getSexo() . PHP_EOL;
    }
}

class extra extends bar
{
    public function printPHP()
    {
        echo 'PHP is wonderful.' . PHP_EOL;
    }
}

$foo = new Foo();
$bar = new Bar();
$extra = new Extra();
$foo->printItem('baz'); // Salida: 'Foo: baz'
$foo->printPHP();       // Salida: 'PHP is great' 
$bar->printItem('baz'); // Salida: 'Bar: baz'
$bar->printPHP();       // Salida: 'PHP is great'
$bar->getPerson();
$extra->printItem('extra');
$extra->printPHP();
$extra->getPerson();
