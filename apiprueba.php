<?php

$host = "172.19.112.62"; /* Host name */
$user = "indra_local"; /* User */
$password = "123456"; /* Password */
$dbname = "reporter"; /* Database name */

$con = mysqli_connect($host, $user, $password, $dbname);
mysqli_set_charset($con, "utf8");

// Check connection
if (!$con) {
	echo json_encode(array('state' => false, 'data' => 'Conexión fallida: ' . mysqli_connect_error()));
} else {
	$sql = "select * from elastic_hosts";
	$resultado = mysqli_query($con, $sql);
	$json = array();

	if ($resultado) {
		//printf("La selección devolvió %d filas.\n", mysqli_num_rows($resultado));

		while ($row = mysqli_fetch_assoc($resultado)) {
	        $row['ip'] = parseDataToArray($row['ip'], '<br>');
	        $row['mac'] = parseDataToArray($row['mac'], '<br>');
	        $json[] = $row;
	    }

		mysqli_free_result($resultado);
		mysqli_close($con);

	    echo json_encode(array('state' => true, 'data' => $json));
	} else {
		echo json_encode(array('state' => false, 'data' => 'Error durante la consulta'));
	}
}

function parseDataToArray($columna, $delimitador)
{
	$columna = explode($delimitador, $columna);
	return $columna;
}
