<?php

use Cron\Exception\InvalidPatternException;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

if (php_sapi_name() != 'cli') {
    exit(1);
}

if(!defined('FCPATH'))
    define('FCPATH', str_replace("\\", "/", __DIR__));

require_once FCPATH . '/vendor/autoload.php';

$jobs = include_once FCPATH . '/config/cron.php';
$jobs = array_merge($jobs);

try {
    $resolver = new \Cron\Resolver\ArrayResolver();
    $logger = new Logger("cron");
    $logger->pushHandler(new StreamHandler('php://stdout'));
    $logger->pushHandler(new RotatingFileHandler(FCPATH . '/cron/logs/cron.log', 5, LOGGER::INFO));

    foreach ($jobs as $job) {
        $time = $job[0];
        $command = $job[1];

        try {
            $schedule = new \Cron\Schedule\CrontabSchedule($time);
        } catch (InvalidPatternException $e) {
            $logger->critical(sprintf("%s, command: %s, time: %s",$e->getMessage(), $command, $time));
            continue;
        }

        $job = new \Cron\Job\ShellJob($schedule);
        $job->setSchedule(new \Cron\Schedule\CrontabSchedule($time));
        $job->setCommand($command);
        $resolver->addJob($job);
    }

    $cron = new \Cron\Cron();
    $cron->setExecutor(new \Cron\Executor\Executor());
    $cron->setResolver($resolver);

    $report = $cron->run();
    
    while ($cron->isRunning()) {
        sleep(1);
    }

    foreach ($report->getReports() as $report) {
        $process = $report->getJob()->getProcess();
        $array = array(
            'exit code' => $process->getExitCode(),
            'start time' => date(DATE_ISO8601, $report->getStartTime()),
            'end time' => date(DATE_ISO8601, $report->getEndTime()),
            'elapsed seconds' => $report->getEndTime() - $report->getStartTime(),
            'error' => $report->getError(),
            'is successful' => $report->isSuccessful()
        );

        $logger->info(sprintf("command line: %s, ",
            $process->getCommandLine()), $array);
    }
} catch (Exception $e) {
    $logger->addCritical($e->getMessage());
}
