<?php
interface a
{
	const b = 'Interface constant';
    
    public function foo();
}

interface b extends a
{
    public function baz(Baz $baz);
}

interface c extends a, b
{
    public function bar();
}

// Ésto sí funcionará
class d implements c
{
    public function foo()
    {
    }

    public function baz(Baz $baz)
    {
    }

    public function bar()
    {
    }
}

echo d::b;
