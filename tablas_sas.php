<?php
$html = "";

$monitor = "Monitoreo_IPFabric_CoreHuawei";
$item = array(
	array(
		'name' => 'IF-192.168.120.13-328514',
		'severidad' => 'Critical',
		'loadtime' => '2019-05-28 17:50:00',
		'device_displayname' => 'VPN_SSL_F5_VES',
		'device_ipaddress' => '192.168.120.13',
		'device_interface' => 'mgmt-mgmt',
		'inoctets' => '135.42Kbps',
		'outoctets' => '3.12Mbps',
		'radio_limite' => '0.85',
		'SVDDDISTANCE' => '1.15',
		'variacion' => '34.10%',
		'enlace' => 'http://172.19.4.190/opmanager_nap/opmanager/reports/index/Monitoreo_RCSR_Plataforma_VPNSSL_F5#mgmt-mgmt#VPN_SSL_F5_VES'
	),
	array(
		'name' => 'IF-192.168.120.13-328514',
		'severidad' => 'Critical',
		'loadtime' => '2019-05-28 17:50:00',
		'device_displayname' => 'VPN_SSL_F5_VES',
		'device_ipaddress' => '192.168.120.13',
		'device_interface' => 'mgmt-mgmt',
		'inoctets' => '135.42Kbps',
		'outoctets' => '3.12Mbps',
		'radio_limite' => '0.85',
		'SVDDDISTANCE' => '1.15',
		'variacion' => '34.10%',
		'enlace' => 'http://172.19.4.190/opmanager_nap/opmanager/reports/index/Monitoreo_RCSR_Plataforma_VPNSSL_F5#mgmt-mgmt#VPN_SSL_F5_VES'
	),
	array(
		'name' => 'IF-192.168.120.13-328514',
		'severidad' => 'Critical',
		'loadtime' => '2019-05-28 17:50:00',
		'device_displayname' => 'VPN_SSL_F5_VES',
		'device_ipaddress' => '192.168.120.13',
		'device_interface' => 'mgmt-mgmt',
		'inoctets' => '135.42Kbps',
		'outoctets' => '3.12Mbps',
		'radio_limite' => '0.85',
		'SVDDDISTANCE' => '1.15',
		'variacion' => '34.10%',
		'enlace' => 'http://172.19.4.190/opmanager_nap/opmanager/reports/index/Monitoreo_RCSR_Plataforma_VPNSSL_F5#mgmt-mgmt#VPN_SSL_F5_VES'
	),
	array(
		'name' => 'IF-192.168.120.13-328514',
		'severidad' => 'Critical',
		'loadtime' => '2019-05-28 17:50:00',
		'device_displayname' => 'VPN_SSL_F5_VES',
		'device_ipaddress' => '192.168.120.13',
		'device_interface' => 'mgmt-mgmt',
		'inoctets' => '135.42Kbps',
		'outoctets' => '3.12Mbps',
		'radio_limite' => '0.85',
		'SVDDDISTANCE' => '1.15',
		'variacion' => '34.10%',
		'enlace' => 'http://172.19.4.190/opmanager_nap/opmanager/reports/index/Monitoreo_RCSR_Plataforma_VPNSSL_F5#mgmt-mgmt#VPN_SSL_F5_VES'
	),
	array(
		'name' => 'IF-192.168.120.13-328514',
		'severidad' => 'Critical',
		'loadtime' => '2019-05-28 17:50:00',
		'device_displayname' => 'VPN_SSL_F5_VES',
		'device_ipaddress' => '192.168.120.13',
		'device_interface' => 'mgmt-mgmt',
		'inoctets' => '135.42Kbps',
		'outoctets' => '3.12Mbps',
		'radio_limite' => '0.85',
		'SVDDDISTANCE' => '1.15',
		'variacion' => '34.10%',
		'enlace' => 'http://172.19.4.190/opmanager_nap/opmanager/reports/index/Monitoreo_RCSR_Plataforma_VPNSSL_F5#mgmt-mgmt#VPN_SSL_F5_VES'
	)
);

$html.= "<h2>Machine Learning Anomaly Alert - $monitor</h2>";

$html.= "<table border='1' height='100%' cellpadding='0' cellmargin='0' style='border:1px solid;font-size:9pt;font-family:Calibri;border-collapse:collapse' width='1500'>";
	$html.= "<tbody>";
			$html.= "<tr style='background:#dddddd;'>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Id";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Dashboard";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Device Displayname";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Device Ipaddress";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Alarm Time";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Severidad";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Device Interface";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Rx Traffic";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Tx Traffic";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Radio Limite";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Radio Actual";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Variacion";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
				$html.= "<td style='border:black solid 1.0pt; background:#dddddd; padding:5px 5px 5px 5px'>";
					$html.="<div align='center' style='text-align:center;margin:0;'>";
						$html.="<font>";
							$html.="<span style='font-size:12pt;'>";
								$html.= "<span style='font-size:9pt'>";
									$html.= "<b>";
										$html.= "Enlace";
									$html.= "</b>";
								$html.= "</span>";
							$html.="</span>";
						$html.="</font>";
					$html.="</div>";
				$html.= "</td>";
			$html.= "</tr>";

		foreach ($item as $key2 => $value2) {
			$name = $value2["name"];
			$severidad = $value2["severidad"];
			$loadtime = $value2["loadtime"];
			$device_displayname = $value2["device_displayname"];
			$device_ipaddress = $value2["device_ipaddress"];
			$device_interface = $value2["device_interface"];
			$inoctets = $value2["inoctets"];
			$outoctets = $value2["outoctets"];
			$radio_limite = $value2["radio_limite"];
			$SVDDDISTANCE = $value2["SVDDDISTANCE"];
			$variacion = $value2["variacion"];
			$enlace = $value2["enlace"];

			$html.= "<tr style='font-size:9pt'>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $name;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $monitor;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $device_displayname;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $device_ipaddress;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $loadtime;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $severidad;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $device_interface;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $inoctets;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $outoctets;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $radio_limite;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $SVDDDISTANCE;
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					$html.= $variacion."%";
				$html.= "</td>";
				$html.= "<td cellpadding='0' cellspacing='0' style='border:black solid 1.0pt; padding:5px 5px 5px 5px;'>";
					if ($enlace) {
						$html.= "<a href='".$enlace."' target='_blank'>Ver enlace</a>";
					} else {
						$html.= "-";
					}
				$html.= "</td>";
			$html.= "</tr>";
		}

	$html.= "</tbody>";
$html.= "</table>";

echo $html;
