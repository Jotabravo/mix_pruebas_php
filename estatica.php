<?php

class Foo {
	public static $mi_static = 'foo';

    public function valorStatic()
    {
        echo self::$mi_static . PHP_EOL;
    }

    public static function unMetodoEstatico()
    {
        echo "Método estático" . PHP_EOL;
    }
}

class Bar extends Foo
{
	public static $mi_static = 'bar';

    public function fooStatic()
    {
        echo self::$mi_static . PHP_EOL;
        echo parent::$mi_static . PHP_EOL;
    }
}

echo "**** 1 **** " . PHP_EOL;

Foo::unMetodoEstatico();

$nombre_clase = 'Foo';
$nombre_clase::unMetodoEstatico(); // A partir de PHP 5.3.0

echo "**** 2 **** " . PHP_EOL;

Foo::valorStatic();
Bar::fooStatic();

echo "**** 3 **** " . PHP_EOL;

print Foo::$mi_static . PHP_EOL;
print Bar::$mi_static . PHP_EOL;

echo "**** 4 **** " . PHP_EOL;

$foo = new Foo();
$foo->valorStatic() . PHP_EOL;
//$foo->mi_static . PHP_EOL; // "Propiedad" mi_static no definida

echo "**** 5 **** " . PHP_EOL;

print $foo::$mi_static . PHP_EOL;
$nombreClase = 'Foo';
print $nombreClase::$mi_static . PHP_EOL; // A partir de PHP 5.3.0

echo "**** 6 **** " . PHP_EOL;

print Bar::$mi_static . PHP_EOL;
$bar = new Bar();
$bar->fooStatic() . PHP_EOL;
