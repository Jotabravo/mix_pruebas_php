<?php

$host = "172.19.112.62"; /* Host name */
$user = "indra_local"; /* User */
$password = "123456"; /* Password */
$dbname = "reporter"; /* Database name */

$con = mysqli_connect($host, $user, $password, $dbname);
mysqli_set_charset($con, "utf8");

// Check connection
if (!$con) {
	echo json_encode(array('state' => false, 'data' => 'Conexión fallida: ' . mysqli_connect_error()));
} else {
	$id = $_GET['id'];

	if (!$id || !is_numeric($id)) {
		echo json_encode(array('state' => false, 'data' => 'No se encontraron resultados'));
	} else {
		$sql = "select * from elastic_hosts where id = ?";
		$stmt = mysqli_prepare($con, $sql);

		if ($stmt) {
			mysqli_stmt_bind_param($stmt, "s", $id);

			mysqli_stmt_execute($stmt);

			$resultado = mysqli_stmt_get_result($stmt);
				
			if ($resultado->num_rows == 0) {
				echo json_encode(array('state' => false, 'data' => 'No se encontraron resultados'));
			} else {
				while ($row = mysqli_fetch_assoc($resultado)) {
					$row['ip'] = parseDataToArray($row['ip'], '<br>');
		        	$row['mac'] = parseDataToArray($row['mac'], '<br>');
		        	$json = $row;
		    	}

			    echo json_encode(array('state' => true, 'data' => $json));
			}

	    	mysqli_stmt_close($stmt);
	    	mysqli_free_result($resultado);
			mysqli_close($con);
		} else {
			echo json_encode(array('state' => false, 'data' => 'Error durante la consulta'));
		}
	}
}

function parseDataToArray($columna, $delimitador)
{
	$columna = explode($delimitador, $columna);
	return $columna;
}
