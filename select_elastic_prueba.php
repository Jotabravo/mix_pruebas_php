<?php
    try {
        $is_elastic = true;
        $number = 2;

        if ($is_elastic) {
            $server_ip = '10.95.164.148';
            $server_port = '9200';
            $server_url = "http://{$server_ip}:{$server_port}";
            $server_user = 'elastic';
            $server_password = 'claro123';
            $index = "log_precargas_produccion-*";
            //$index = "log_precargas_produccion-*";

            $dt_start = strtotime("-15 min")*1000; //Tiempo inicial para query hacia el API Elastic S.
            $dt_end = strtotime("now")*1000;
            $tipo_auditoria = "1";
            $servicio_id = "/gestionpagosrs/resources/pagos/pagarProducto";
            $tipo_mensaje = "10";
            $mensaje = "idMetodoPago:2";
            $size = 10000; //Máximo soportado por Kibana

            $config = array(
                'method' => 'GET',
                'postFields' => array(
                    'query' => array(
                        'bool' => array(
                            'must' => array(
                                array(
                                    'match_phrase' => array(
                                        'tipo_auditoria' => $tipo_auditoria
                                    )
                                ),
                                array(
                                    'match_phrase' => array(
                                        'servicio_id' => $servicio_id
                                    )
                                ),
                                array(
                                    'match_phrase' => array(
                                        'tipo_mensaje' => $tipo_mensaje
                                    )
                                ),
                                array(
                                    'match_phrase' => array(
                                        'mensaje' => $mensaje
                                    )
                                ),
                                array(
                                    'range' => array(
                                        '@timestamp' => array(
                                            'gte' => $dt_start,
                                            'lte' => $dt_end,
                                            'relation' => 'within'
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    'size' => $size
                ),
                'output' => 'array',
                'headers' => 'json'
            );

            $url = "$server_url/$index/_search?pretty";

            $hasConfig = isset($config) && !empty($config);
            $hasPostFields = $hasConfig && isset($config['postFields']);
            $hasHeaders = $hasConfig && isset($config['headers']);
            $hasMethod = $hasConfig && isset($config['method']);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            if ($hasPostFields)
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['postFields']));

            if ($hasMethod)
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($config['method']));
            else
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

            curl_setopt($ch, CURLOPT_USERPWD, "{$server_user}:{$server_password}");

            if ($hasHeaders) {
                if (is_string($config['headers']) && $config['headers'] === 'json') {
                    $headers = array("Content-Type: application/json");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                } elseif (is_array($config['headers'])) {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $config['headers']);
                }
            }

            $result = curl_exec($ch);

            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch) . PHP_EOL;
            }

            curl_close($ch);

            $result = json_decode($result, true);

            $buckets = isset($result["hits"]["hits"]) ? $result["hits"]["hits"] : array();
            $total_buckets = count($buckets);

            if ($total_buckets > 0) {
                echo "$total_buckets elementos encontrados" . PHP_EOL;
                echo PHP_EOL;

                foreach ($buckets as $key => &$value) {
                    var_dump($value);
                }
            } else {
                echo "No se encontraron resultados";
            }
        } else {
            if ($number < 2) {
                $url = "https://jsonplaceholder.typicode.com/todos/";

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

                $headers = array("Content-Type: application/json");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $result = curl_exec($ch);

                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch) . PHP_EOL;
                }

                curl_close($ch);

                $result = json_decode($result, true);
                var_dump($result);
            } else {
                $server_ip = '172.16.102.101';
                $server_port = '9200';
                $server_url = "http://{$server_ip}:{$server_port}";
                $server_user = 'elastic';
                $server_password = 'claro123';
                $index = "base-cei-usr-*";

                $size = 10000; //Máximo soportado por Kibana

                $config = array(
                    'method' => 'GET',
                    'postFields' => array(
                        'query' => array(
                            'bool' => array(
                                'must' => array(
                                )
                            )
                        ),
                        'size' => $size
                    ),
                    'output' => 'array',
                    'headers' => 'json'
                );

                $url = "$server_url/$index/_search?pretty";

                $hasConfig = isset($config) && !empty($config);
                $hasPostFields = $hasConfig && isset($config['postFields']);
                $hasHeaders = $hasConfig && isset($config['headers']);
                $hasMethod = $hasConfig && isset($config['method']);
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                if ($hasPostFields)
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['postFields']));

                if ($hasMethod)
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($config['method']));
                else
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

                curl_setopt($ch, CURLOPT_USERPWD, "{$server_user}:{$server_password}");

                if ($hasHeaders) {
                    if (is_string($config['headers']) && $config['headers'] === 'json') {
                        $headers = array("Content-Type: application/json");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    } elseif (is_array($config['headers'])) {
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $config['headers']);
                    }
                }

                $result = curl_exec($ch);

                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch) . PHP_EOL;
                }

                curl_close($ch);

                $result = json_decode($result, true);

                $buckets = isset($result["hits"]["hits"]) ? $result["hits"]["hits"] : array();
                $total_buckets = count($buckets);

                if ($total_buckets > 0) {
                    echo "$total_buckets elementos encontrados" . PHP_EOL;
                    echo PHP_EOL;

                    foreach ($buckets as $key => $value) {
                        $source = $value["_source"];
                        $timestamp = $source["@timestamp"];
                        $day = $source["day"];

                        var_dump($timestamp, $day);
                    }
                } else {
                    echo "No se encontraron resultados";
                }
            }
        }
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
