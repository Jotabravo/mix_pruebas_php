<?php

## Database configuration
include 'config.php';

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (ID like '%".$searchValue."%' or 
        APPID like '%".$searchValue."%' or 
        APPLICATION like'%".$searchValue."%' or
        CATEGORY like'%".$searchValue."%' or
        PROT_TYPE like'%".$searchValue."%' or
        fecha like'%".$searchValue."%' ) ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"select count(*) as allcount from di_apps_prot_mapping");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($con,"select count(*) as allcount from di_apps_prot_mapping WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from di_apps_prot_mapping WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($con, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
   $data[] = array( 
      "ID"=>$row['ID'],
      "APPID"=>$row['APPID'],
      "APPLICATION"=>$row['APPLICATION'],
      "CATEGORY"=>$row['CATEGORY'],
      "PROT_TYPE"=>$row['PROT_TYPE'],
      "fecha"=>$row['fecha']
   );
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data
);

$json_response = json_encode($response);

echo $json_response;
