<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<!-- Datatable CSS -->
		<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<!-- Table -->
		<table id='empTable' class='display dataTable'>
			<thead>
				<tr>
					<th>ID</th>
					<th>APPID</th>
					<th>APPLICATION</th>
					<th>CATEGORY</th>
					<th>PROT_TYPE</th>
					<th>fecha</th>
				</tr>
			</thead>
		</table>
	</body>
	<!-- jQuery Library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Datatable JS -->
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#empTable').DataTable({
				'processing': true,
				'serverSide': true,
				'serverMethod': 'post',
				'ajax': {
					'url':'ajaxfile.php'
				},
				'columns': [
					{ data: 'ID' },
					{ data: 'APPID' },
					{ data: 'APPLICATION' },
					{ data: 'CATEGORY' },
					{ data: 'PROT_TYPE' },
					{ data: 'fecha' },
				]
			});
		});
	</script>
</html>
