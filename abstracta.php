<?php
abstract class ClaseAbstracta
{
    // Forzar la extensión de clase para definir este método
    abstract protected function getValor();
    abstract protected function valorPrefijo($prefijo);
    abstract protected function otro();
    abstract protected function nombrePrefijo($nombre);

    // Método común
    public function imprimir() {
        print $this->getValor() . "\n";
    }
}

class ClaseConcreta1 extends ClaseAbstracta
{
    protected function getValor() {
        return "ClaseConcreta1";
    }

    public function valorPrefijo($prefijo) {
        return "{$prefijo}ClaseConcreta1";
    }

    public function otro() {
        echo "1" . PHP_EOL;
    }

    public function nombrePrefijo($nombre, $separador = ".") {
        if ($nombre == "Pacman") {
            $prefijo = "Mr";
        } elseif ($nombre == "Pacwoman") {
            $prefijo = "Mrs";
        } else {
            $prefijo = "";
        }
        return "{$prefijo}{$separador} {$nombre}";
    }
}

class ClaseConcreta2 extends ClaseAbstracta
{
    public function getValor() {
        return "ClaseConcreta2";
    }

    public function valorPrefijo($prefijo) {
        return "{$prefijo}".$this->getValor();
    }

    public function otro() {
        echo "2" . PHP_EOL;
    }

    public function nombrePrefijo($nombre, $separador = ".") {
        if ($nombre == "Pacman") {
            $prefijo = "Mr";
        } elseif ($nombre == "Pacwoman") {
            $prefijo = "Mrs";
        } else {
            $prefijo = "";
        }
        return "{$prefijo}{$separador} {$nombre}";
    }
}

$clase1 = new ClaseConcreta1;
$clase1->imprimir();
echo $clase1->valorPrefijo('FOO_') ."\n";
$clase1->otro();
echo $clase1->nombrePrefijo("Pacman"), "\n";

$clase2 = new ClaseConcreta2;
$clase2->imprimir();
echo $clase2->valorPrefijo('FOO_') ."\n";
$clase2->otro();
echo $clase2->nombrePrefijo("Pacwoman"), "\n";
